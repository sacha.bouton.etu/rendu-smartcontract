Un contrat intelligent de vote peut varier de simple à complexe, en fonction des besoins des élections que vous voulez organiser. Le vote peut concerner un nombre restreint de propositions (ou de candidats) prédéterminées ou un grand nombre de propositions soumises dynamiquement par les votants eux-mêmes.

Dans ce contexte, vous allez rédiger un contrat intelligent de vote pour une petite organisation. Les votants, tous connus de l'organisation, sont inscrits sur une liste blanche (whitelist) à l'aide de leur adresse Ethereum, peuvent proposer de nouvelles idées lors d'une session d'enregistrement des propositions et peuvent voter sur les propositions lors de la session de vote.

✔️ Le vote n'est pas confidentiel pour les utilisateurs ajoutés à la liste blanche.
✔️ Chaque votant peut consulter les votes des autres.
✔️ Le vainqueur est déterminé à la majorité simple.
✔️ La proposition qui reçoit le plus de votes l'emporte.

👉 **Le processus de vote :**

Voici comment se déroule l'ensemble du processus de vote :

- L'administrateur du vote inscrit une liste blanche d'électeurs identifiés par leur adresse Ethereum.
- L'administrateur du vote démarre la session d'enregistrement des propositions.
Les électeurs inscrits peuvent soumettre leurs propositions pendant que la session d'enregistrement est active.
- L'administrateur du vote clôture la session d'enregistrement des propositions.
- L'administrateur du vote lance la session de vote.
- Les électeurs inscrits votent pour leur proposition favorite.
- L'administrateur du vote clôture la session de vote.
- L'administrateur du vote comptabilise les votes.
- Tout le monde peut vérifier les derniers détails de la proposition gagnante.
